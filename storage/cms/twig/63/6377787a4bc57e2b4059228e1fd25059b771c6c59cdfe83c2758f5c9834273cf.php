<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/plugins/marci/demo/components/savings/default.htm */
class __TwigTemplate_91cbec5b437d749971367fb6480ba3479995a30ef235055c0f5438e64a9acec0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("put" => 7);
        $filters = array("escape" => 3, "theme" => 8);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['put'],
                ['escape', 'theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<form
    role=\"form\"
    data-request=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["__SELF__"] ?? null), 3, $this->source), "html", null, true);
        echo "::onAddItem\"
    data-request-update=\"'";
        // line 4
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["__SELF__"] ?? null), 4, $this->source), "html", null, true);
        echo "::list': '#result'\"
    data-request-success=\"\$('#input-name').val('')\"
    data-request-flash>
    ";
        // line 7
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('styles'        );
        // line 8
        echo "        <link href=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/mystyle.css");
        echo "\" rel=\"stylesheet\" />
    ";
        // line 7
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 10
        echo "    <link href=\"mystyle.css\" rel=\"stylesheet\" />
    <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
            <h3 class=\"panel-title\">Bevételek és kiadások listája</h3>
        </div>
        <div class=\"panel-body\">
            <div class=\"input-group\">
                <div class=\"row\">
                    <label for=\"input-name\" class=\"label\">Név</label>
                    <input type=\"text\" id=\"input-name\" class=\"form-input\" value=\"\" name=\"name\" placeholder=\"Név\">
                </div>
                <div class=\"row\">
                    <label for=\"input-desc\" class=\"label\">Leírás</label>
                    <input type=\"text\" id=\"input-desc\" class=\"form-input\" value=\"\" name=\"description\" placeholder=\"Leírás\">
                </div>
                <div class=\"row\">
                    <label for=\"input-date\" class=\"label\">Dátum</label>
                    <input type=\"date\" id=\"input-date\" class=\"form-input\" value=\"\" name=\"date\" placeholder=\"\">
                </div>
                <input type=\"radio\" id=\"income\" name=\"age\" value=\"income\" checked=\"checked\">
                <label for=\"income\">Bevétel</label>
                <input type=\"radio\" id=\"expense\" name=\"age\" value=\"expense\">
                <label for=\"expense\">Kiadás</label>
                <div class=\"row\">
                    <label for=\"input-value\" class=\"label\">Érték</label>
                    <input type=\"text\" id=\"input-value\" class=\"form-input\" value=\"\" name=\"value\" placeholder=\"Érték\">
                </div>
            
                <!-- IDE JÖN A DEVIZAVÁLASZTÓ-->

                <button type=\"submit\" class=\"btn btn btn-primary\" data-attach-loading>Add</button>
            
                <!--span class=\"input-group-btn\">
                    <button type=\"submit\" class=\"btn btn btn-primary\" data-attach-loading>Add</button>
                </span-->
            </div>
        </div>
        <ul class=\"list-group\" id=\"result\">
        </ul>
    </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/plugins/marci/demo/components/savings/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 10,  83 => 7,  78 => 8,  76 => 7,  70 => 4,  66 => 3,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form
    role=\"form\"
    data-request=\"{{ __SELF__ }}::onAddItem\"
    data-request-update=\"'{{ __SELF__ }}::list': '#result'\"
    data-request-success=\"\$('#input-name').val('')\"
    data-request-flash>
    {% put styles %}
        <link href=\"{{ 'assets/css/mystyle.css'|theme }}\" rel=\"stylesheet\" />
    {% endput %}
    <link href=\"mystyle.css\" rel=\"stylesheet\" />
    <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
            <h3 class=\"panel-title\">Bevételek és kiadások listája</h3>
        </div>
        <div class=\"panel-body\">
            <div class=\"input-group\">
                <div class=\"row\">
                    <label for=\"input-name\" class=\"label\">Név</label>
                    <input type=\"text\" id=\"input-name\" class=\"form-input\" value=\"\" name=\"name\" placeholder=\"Név\">
                </div>
                <div class=\"row\">
                    <label for=\"input-desc\" class=\"label\">Leírás</label>
                    <input type=\"text\" id=\"input-desc\" class=\"form-input\" value=\"\" name=\"description\" placeholder=\"Leírás\">
                </div>
                <div class=\"row\">
                    <label for=\"input-date\" class=\"label\">Dátum</label>
                    <input type=\"date\" id=\"input-date\" class=\"form-input\" value=\"\" name=\"date\" placeholder=\"\">
                </div>
                <input type=\"radio\" id=\"income\" name=\"age\" value=\"income\" checked=\"checked\">
                <label for=\"income\">Bevétel</label>
                <input type=\"radio\" id=\"expense\" name=\"age\" value=\"expense\">
                <label for=\"expense\">Kiadás</label>
                <div class=\"row\">
                    <label for=\"input-value\" class=\"label\">Érték</label>
                    <input type=\"text\" id=\"input-value\" class=\"form-input\" value=\"\" name=\"value\" placeholder=\"Érték\">
                </div>
            
                <!-- IDE JÖN A DEVIZAVÁLASZTÓ-->

                <button type=\"submit\" class=\"btn btn btn-primary\" data-attach-loading>Add</button>
            
                <!--span class=\"input-group-btn\">
                    <button type=\"submit\" class=\"btn btn btn-primary\" data-attach-loading>Add</button>
                </span-->
            </div>
        </div>
        <ul class=\"list-group\" id=\"result\">
        </ul>
    </div>
</form>
", "/var/www/html/plugins/marci/demo/components/savings/default.htm", "");
    }
}

<?php namespace Marci\Demo;

/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Savings',
            'description' => 'Manages savings.',
            'author'      => 'Jenei Márton',
            'icon'        => 'icon-usd'
        ];
    }

    public function registerComponents()
    {
        return [
            '\Marci\Demo\Components\Savings' => 'savings'
        ];
    }
}

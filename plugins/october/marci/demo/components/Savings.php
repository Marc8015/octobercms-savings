<?php namespace Marci\Demo\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;



class Savings extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Savings list',
            'description' => 'Implements a savings list.'
        ];
    }

    public function defineProperties()
    {
        return [
            /*
            'max' => [
                'description'       => 'The most amount of todo items allowed',
                'title'             => 'Max items',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max Items value is required and should be integer.'
            ]
            */
        ];
    }

    public function onAddItem()
    {
        //$items = post('items', []);

        $item = new \stdClass();

        $item->name = \Input::get("name");
        $item->desc = \Input::get("description");
        $item->value = \Input::get("value");
        $item->date = \Input::get("date");
        $item->incomeexpense = \Input::get("incomeexpense");

        $items[] = $item;

        //var_dump($item);

        $this->page['items'] = $items;
    }
}

<?php namespace Marci\Demo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMarciDemoItems extends Migration
{
    public function up()
    {
        Schema::create('marci_demo_items', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->date('date');
            $table->boolean('isexpense');
            $table->integer('value');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('marci_demo_items');
    }
}

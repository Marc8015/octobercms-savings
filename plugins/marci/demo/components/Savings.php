<?php namespace Marci\Demo\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;
use Marci\Demo\Models\Item;



class Savings extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Savings list',
            'description' => 'Implements a savings list.'
        ];
    }

    public function defineProperties()
    {
        return [
            /*
            'max' => [
                'description'       => 'The most amount of todo items allowed',
                'title'             => 'Max items',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max Items value is required and should be integer.'
            ]
            */
        ];
    }

    public function items() {
        return Item::all();
    }


    public function onAddItem()
    {
        $item = new Item;
        $item->name = post('name');
        $item->description = post('description');
        $item->date = post('date');
        if(!strcmp(post('incomeexpense'), 'expense')) {
            $item->isexpense = true;
        }
        else {
            $item->isexpense = false;
        }
        $item->value = post('value');

        $validatedItem = $item->validate([
            'name' => 'required|max:255',
            'date' => 'required',
            'value' => 'required|numeric'
        ]);

        $item->save();
    }
}
